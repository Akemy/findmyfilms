# Find my films #

### What is this repository for? ###

This project is created because of my thesis at University of Debrecen.

The web application is a film management system. It scans the computer for films, download informations from IMDb and show them in the browser to help to find one to see. You can rate them and play with a desktop media player.

### Run the application ###

You need Python 2.7 with these packages:

* flask==0.10.1 
* flask_wtf==0.12 
* pymongo==3.2.2 
* imdbpy==5.0 
* guessit==0.10.4

### First use ###

1. Run the application with ```python main.py```

2. You can reach it in localhost: http://localhost:5000/

3. In the settings page you have to add a connection link of a MongoDB database. This can be a local or in the cloud.

4. In the settings page add path(s) of film directories. It's important how the path must contains subdirectories. These subdirectories contains the files of a film. See the example below.

5. In the settings page add your media player's path if you want to start the films from your browser. If it is VLC or KMPlayer, you can use the timer.

6. Save settings and refresh the database with the button in the menu. This can take for minutes.


For example you can give the My_films directory which looks like this scheme:
```
My_films/
|
---- Movie1/
     |
     ---- movie1.mkv
     ---- info.nfo
|
---- Movie2/
     |
     ---- movie2.mkv
```