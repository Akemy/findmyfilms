# -*- coding: utf-8 -*-

from flask import Flask
from random_string import RandomString
from views import FindFilmsIndex, FindMyFilms, Search, Settings, UpdateAll, UpdateNew, NotSure, Play, Save


app = Flask(__name__)
app.config["SECRET_KEY"] = RandomString.get_random_string()

app.add_url_rule('/', view_func=FindFilmsIndex.as_view("find_films_index"), methods=['GET'])
app.add_url_rule('/findmyfilms', view_func=FindMyFilms.as_view("find_my_films"), methods=['GET'])
app.add_url_rule('/search', view_func=Search.as_view("search"), methods=['POST'])
app.add_url_rule('/settings', view_func=Settings.as_view("settings"), methods=['GET', 'POST'])
app.add_url_rule('/update-all', view_func=UpdateAll.as_view("update_all"), methods=['GET'])
app.add_url_rule('/update-new', view_func=UpdateNew.as_view("new"), methods=['GET'])
app.add_url_rule('/not-sure', view_func=NotSure.as_view("not_sure"), methods=['GET', 'POST'])
app.add_url_rule('/play', view_func=Play.as_view("play"), methods=['POST'])
app.add_url_rule('/save', view_func=Save.as_view("save"), methods=['POST'])

if __name__ == "__main__":
    app.run(debug=True)