'''
Get important info from search result.
'''

import os
import urllib


class Movie:
    movie = {}
    data_in_list = {}

    def __init__(self):
        self.movie = {}

    def set_location(self, location):
        self.movie['path'] = location#"/".join(location.split("\\"))

    def set_dict_from_db(self, dict):
        self.movie = dict

    def set_is_id_valid(self, isIdSafe):
        self.movie['isidvalid'] = isIdSafe

    def set_video_files(self, video_files_in_dir):
        self.movie['videofiles'] = video_files_in_dir

    def set_movie_info(self, movie_data):
        self.movie['imdbid'] = movie_data.movieID

        self.movie['imdblink'] = "http://www.imdb.com/title/tt" + self.movie['imdbid'] + "/"

        self.movie['title'] = movie_data.get('title')

        self.movie['genres'] = movie_data.get('genres')

        self.movie['director'] = []
        if "director" in movie_data.keys():
            for director in movie_data.get('director'):
                self.movie['director'].append(director['name'])

        self.movie['writer'] = []
        if "writer" in movie_data.keys():
            for writer in movie_data.get('writer'):
                self.movie['writer'].append(writer['name'])

        self.movie['cast'] = []
        if "cast" in movie_data.keys():
            for cast in movie_data.get('cast'):
                self.movie['cast'].append(cast['name'])

        self.movie['runtime'] = movie_data.get('runtime')

        self.movie['country'] = movie_data.get('country')

        self.movie['language'] = movie_data.get('language')

        self.movie['rating'] = movie_data.get('rating')

        self.movie['plot'] = movie_data.get('plot')

        self.movie['year'] = movie_data.get('year')

        if "full-size cover url" in movie_data.keys():
            self.movie['cover'] = movie_data['full-size cover url']
        else:
            self.movie['cover'] = "None"

        self.getImg()

    def set_imdbid(self, imdbid):
        self.movie['imdbid'] = imdbid

    def set_cover(self, cover):
        self.movie['cover'] = cover

    def getImg(self):
        if not os.path.isfile("static/cover/temp"+self.movie['imdbid'] + ".jpg"):
            if self.movie['cover'] != "None":
                image = urllib.URLopener()
                image.retrieve(self.movie['cover'], "static/cover/temp/" + self.movie['imdbid'] + ".jpg")
