import ast
import os
import thread
import subprocess
import json

from config import connection_link_file_name, film_paths_file_name, player_file_name
from mongodb import MongoDB
from find_my_films import findMyFilms
from movie import Movie


class Service:
    connection_link = ""
    film_paths = []
    player = ""
    update_in_progress = False

    def __init__(self):
        with open("static/"+film_paths_file_name) as paths:
            try:
                self.film_paths = ast.literal_eval(paths.read())
            except:
                self.film_paths = []
        with open("static/"+connection_link_file_name) as conn:
            self.connection_link = conn.read().strip()
        with open("static/"+player_file_name) as play:
            self.player = play.read().strip()
        self.MongoDB = MongoDB(self.connection_link)

    def invalid_to_valid(self, validFilmList):
        for film in validFilmList:
            findMyFilms.place_imdb_link_file(film)
        self.update_path([film["path"] for film in validFilmList], True)

    def update_connection_link(self, connection_link):
        print connection_link, " vs ", self.connection_link
        if connection_link != self.connection_link:
            with open("static/"+connection_link_file_name, "w" ) as f:
                f.write(connection_link)
                self.connection_link = connection_link
                self.MongoDB.init_connection_link(self.connection_link)

    def update_film_paths(self, paths):
        pathslist = [str(item['name']) for item in paths]
        print pathslist, " vs ", self.film_paths
        if pathslist != self.film_paths:
            with open("static/"+film_paths_file_name, "w" ) as f:
                f.write(str(pathslist))
                self.film_paths = pathslist

    def update_player(self, player):
        if player != self.player:
            with open("static/"+player_file_name, "w") as f:
                f.write(player)
                self.player = player

    def get_connection_link(self):
        return self.connection_link

    def get_film_paths(self):
        return self.film_paths

    def get_player(self):
        return self.player

    def play(self, file):
        print self.player + " " + file["film"]
        if self.player.lower().find("vlc"):
            time = float(file["timer"]) * 60.0
            subprocess.call([self.player, file["film"], "--start-time={}".format(time)])
        elif self.player.lower().find("kmplayer"):
            time = str(file["timer"] * 60)
            subprocess.call([self.player, file["film"], "/start ", time])
        else:
            subprocess.call([self.player, file["film"]])

    def update_paths(self, paths, overwrite):
        for path in paths:
            findMyFilms.map_paths(path, overwrite)
        findMyFilms.wait()
        self.drop_documents_based_on_locs(paths)
        for path in paths:
            for dirName in [os.path.join(path,o) for o in os.listdir(path) if os.path.isdir(os.path.join(path,o))]:
                self.MongoDB.add_docs_based_on_root(dirName)
        self.update_in_progress = False
        print "DONE Update"

    def update_path(self, paths, overwrite):
        for path in paths:
            print(path)
            findMyFilms.map_path(path, overwrite)
        findMyFilms.wait()
        self.drop_documents_based_on_locs(paths)
        for path in paths:
            self.MongoDB.add_docs_based_on_root(path)
        self.update_in_progress = False
        print "DONE Update"

    def is_available(self, path):
        return os.path.exists(os.path.normpath(path))

    def get_available_locs(self):
        result = []
        for loc in self.get_film_paths():
            if self.is_available(loc):
                result.append(os.path.normpath(loc))
        return result

    def update_all(self, overwrite):
        available_locs = self.get_available_locs()
        self.update_in_progress = True
        thread.start_new_thread(self.update_paths, (available_locs, overwrite)) #self.update_paths(available_locs, overwrite)

    def drop_documents_based_on_locs(self, available_locs):
        self.MongoDB.drop_documents_based_on_locs(available_locs)

    def get_not_sure_films(self, all):
        data = self.MongoDB.get_not_sure_docs(all)
        for item in data:
            item["changed"] = False
        return data

    def update_film_file(self, data):
        data.pop("_id", None)
        data.pop("available", None)
        if data["isidvalid"]:
            with open(os.path.join(data["path"], "info.json"), "w") as f:
                f.write(json.dumps(data))
        else:
            with open(os.path.join(data["path"], "temp.json"), "w") as f:
                f.write(json.dumps(data))
        self.MongoDB.update_video_seen(data)

    def download_images(self, listed_films):
        for film in listed_films:
            if "cover" in film.keys():
                movie = Movie()
                movie.set_imdbid(film["imdbid"])
                movie.set_cover(film["cover"])
                movie.getImg()


service = Service()
