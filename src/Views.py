from flask import render_template, jsonify, request
import flask.views
import json

from forms import IndexForm
from service import service
import thread


class FindFilmsIndex(flask.views.MethodView):
    def get(self):
        form = IndexForm()
        form.create_listed_films({})
        data = {}
        data['listedFilms'] = form.listed_films
        data['categories'] = form.categories
        data['paths'] = form.paths

        return render_template('index.html', form=form)


class FindMyFilms(flask.views.MethodView):
    def get(self):
        form = IndexForm()
        form.create_listed_films({})
        data = {}
        data['listedFilms'] = form.listed_films
        data['categories'] = form.categories
        data['paths'] = form.paths
        data['pageAll'] = form.page_all
        data['pageActual'] = form.page_actual
        return jsonify(data)


class Search(flask.views.MethodView):
    def post(self):
        data = json.loads(request.data)
        form = IndexForm()
        form.create_listed_films(data["search"])
        thread.start_new_thread(service.download_images, (form.listed_films,))
        return jsonify({"listedFilms": form.listed_films,
                        "pageAll": form.page_all,
                        "pageActual": form.page_actual})


class Settings(flask.views.MethodView):
    def get(self):
        print service.get_film_paths()
        return jsonify({"connectionLink": service.get_connection_link(),
                        "paths": [{"name":path} for path in service.get_film_paths()],
                        "player": service.get_player()})

    def post(self):
        data = json.loads(request.data)
        service.update_film_paths(data["paths"])
        service.update_connection_link(data["connectionLink"])
        service.update_player(data["player"])
        return jsonify({"status": "success"})


class UpdateAll(flask.views.MethodView):
    def get(self):
        print "update-all yes yes i am working yes....."
        service.update_all(True)
        print "update-all done"
        return jsonify({"status": "success"})


class UpdateNew(flask.views.MethodView):
    def get(self):
        print "update-new just"
        service.update_all(False)
        print "update-new done"
        return jsonify({"status": "success"})


class NotSure(flask.views.MethodView):
    def get(self):
        all = request.args.get('all')
        return jsonify({"filmList": service.get_not_sure_films(all)})

    def post(self):
        validFilmList = json.loads(request.data)["validFilmList"]
        service.invalid_to_valid(validFilmList)
        return jsonify({"status": "success"})


class Play(flask.views.MethodView):
    def post(self):
        service.play(json.loads(request.data)["play"])
        return jsonify({"status": "success"})


class Save(flask.views.MethodView):
    def post(self):
        data = json.loads(request.data)["file"]
        service.update_film_file(data)
        return jsonify({"status": "success"})
