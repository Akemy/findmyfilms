import os
import thread
import time
import json
from imdb import IMDb
from guessit import guess_video_info

from movie import Movie


class FindMyFilms:
    video_formats = ["aaf", "3gp", "gif", "asf", "avchd", "avi", "cam", "dat", "dsh", "flv", "m1v", "m2v", "fla",
                     "flr", "sol", "m4v", "mkv", "mp4", "wrap", "mng", "mov", "mpeg", "mpg", "mpe", "mxf", "roq", "nsv",
                     "ogg", "rm", "svi", "wmv"]
    json_fine_file_name = "info.json"
    temp_json_file_name = "temp.json"
    imdb_link_file_name = "imdblink.txt"
    thread_count = 0

    def place_imdb_link_file(self, film):
        with open(os.path.normpath(film["path"] + "/" + self.imdb_link_file_name), "w+") as f:
            f.write(film["imdblink"])

    def search_failed(self, root, video_files_in_dir):
        data = {}
        data["path"] = root#"/".join(root.split("\\"))
        data["videofiles"] = video_files_in_dir
        data["imdblink"] = ""
        data["isidvalid"] = False
        data["imdbid"] = "Unknown"
        outfile = open(os.path.join(root, self.temp_json_file_name), 'w')
        try:
            outfile.write(json.dumps(data))
        except:
            print(data)
        outfile.close()
        self.thread_count -= 1
        print "thread done, unknown", root

    def create_file(self, root, winner, video_files_in_dir, isIdSafe):
        movie = Movie()
        movie.set_movie_info(winner)
        movie.set_location(root)
        movie.set_video_files(video_files_in_dir)
        movie.set_is_id_valid(isIdSafe)
        if isIdSafe:
            outfile = open(os.path.join(root,self.json_fine_file_name), 'w')
            outfile.write(json.dumps(movie.movie))
            outfile.close()
        else:
            outfile = open(os.path.join(root, self.temp_json_file_name), 'w')
            outfile.write(json.dumps(movie.movie))
            outfile.close()

    def find_by_id(self, root, _id, video_files_in_dir, isIdSafe):
        asd = IMDb()
        movie_data = asd.get_movie(_id)
        self.create_file(root, movie_data, video_files_in_dir, isIdSafe)
        self.thread_count -= 1
        print "thread done ", root

    def find_by_title(self, root, video_files):
        file_name = [os.path.basename(file["film"]) for file in video_files if os.path.getsize(file["film"]) ==
                     max([os.path.getsize(f["film"]) for f in video_files])][0].decode("utf-8", "ignore")
        video_info = guess_video_info(file_name)
        asd = IMDb()
        _id = ""
        if video_info["type"] == "video":
            print "type = video, maybe i have some problem: ", root
            temp = video_info["title"]  # + " (" + str(video_info["year"]) + ")"
            movie_data = asd.search_movie(temp)
            print movie_data
            # _id = [item for item in movie_data if item.has_key("year") and video_info.has_key("year") and item["year"] == video_info["year"]][0].getID()
            if len(movie_data) > 0:
                _id = movie_data[0].getID()
        elif video_info["type"] == "episode":
            temp = video_info["series"] # episodeNumber, season
            episode_data = asd.search_movie(temp)
            if len(episode_data) > 0:
                _id = episode_data[0].getID()
        if _id == "":
            self.search_failed(root, video_files)
        else:
            self.find_by_id(root, _id, video_files, isIdSafe=False)

    def get_imdb_id_from_file(self, root, file_name):
        _id = "null"
        with open(os.path.join(root, file_name)) as text_file:
            content = text_file.readlines()
            for line in content:
                if "http://www.imdb.com/title/" in line:
                    index = line.rfind("title/tt")
                    _id = line[index+8:index+15]
        return _id

    def search(self, path, video_files):
        search_success = False
        if self.imdb_link_file_name in os.listdir(path):
            if self.temp_json_file_name in os.listdir(path):
                os.remove(os.path.join(path, self.temp_json_file_name))
            _id = self.get_imdb_id_from_file(path, self.imdb_link_file_name)
            if _id != "null":
                self.thread_count += 1
                thread.start_new_thread(self.find_by_id, (path, _id, video_files, True))
        else:
            for f in os.listdir(path):
                if f.endswith(".nfo") or f.endswith(".txt"):
                    _id = self.get_imdb_id_from_file(path, f)
                    if _id != "null":
                        self.thread_count += 1
                        thread.start_new_thread(self.find_by_id, (path, _id, video_files, True))
                        search_success = True
            if not search_success:
                self.thread_count += 1
                thread.start_new_thread(self.find_by_title, (path, video_files))

    def get_video_files(self, path):
        video_files = []
        changed = False
        if self.json_fine_file_name in os.listdir(path):
            with open(os.path.join(path, self.json_fine_file_name), "r") as json_file:
                data = json.load(json_file)
                video_files = data["videofiles"]
        elif self.temp_json_file_name in os.listdir(path):
            with open(os.path.join(path, self.temp_json_file_name), "r") as json_file:
                data = json.load(json_file)
                video_files = data["videofiles"]
        actual_video_files = []
        for root, dirNames, fileNames in os.walk(path):
            actual_video_files += [{"film": os.path.join(root, fileName), "timer": 0, "seen": False}
                            for fileName in fileNames if fileName[fileName.rfind(".")+1:] in self.video_formats]
            for actual in actual_video_files:
                if video_files and actual["film"] in [video["film"] for video in video_files]:
                    actual["timer"] = [film["timer"] for film in video_files if film["film"] == actual["film"]][0]
                    actual["seen"] = [film["seen"] for film in video_files if film["film"] == actual["film"]][0]
        if video_files != actual_video_files:
            changed = True
        return actual_video_files, changed


    def map_paths(self, main_path, overwrite=False):
        for directory in [os.path.normpath(directory) for directory in os.listdir(main_path)
                          if os.path.isdir(os.path.join(main_path, directory))]:
            path = os.path.join(main_path, directory)
            self.map_path(path, overwrite)
        self.wait()
            
    def map_path(self, path, overwrite=False):
        video_files, changed = self.get_video_files(path)
        if len(video_files) > 0:
            if self.json_fine_file_name not in os.listdir(path) and self.temp_json_file_name not in os.listdir(path):
                self.search(path, video_files)
            elif overwrite:
                self.search(path, video_files)
            elif changed:
                self.search(path, video_files)

    def wait(self):
        while self.thread_count > 0:
            time.sleep(1)

findMyFilms = FindMyFilms()
