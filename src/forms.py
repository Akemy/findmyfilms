from flask_wtf import Form
import os

from service import service


class IndexForm(Form):
    listed_films = []
    keywords = ""
    paths = []
    categories = []
    num_of_movies = 12
    page_actual = 1
    page_all = 1

    def __init__(self):
        super(IndexForm, self).__init__()
        self.listed_films = []
        self.paths = []
        for path in service.get_film_paths():
            if path in service.get_available_locs():
                self.paths.append({"path": path, "value": False, "available": True})
            else:
                self.paths.append({"path": path, "value": False, "available": False})
        self.categories = []
        for category in service.MongoDB.get_categories():
            self.categories.append({"category": category, "value": False})

    def create_listed_films(self, data):
        query = {}
        sortby = {}
        if ("paths" in data.keys()) and len(data["paths"]):
            paths = [str(os.path.normpath(path["path"])).encode("string-escape") for path in data["paths"]
                     if path["value"] == True]
            if len(paths):
                query["path"] = {"$regex": "(" + "|".join(paths) + ")"}
        if "categories" in data.keys() and len(data["categories"]):
            categories = [category["category"] for category in data["categories"] if category["value"] == True]
            if len(categories):
                query["genres"] = {"$all": categories}

        for field in ["title", "cast", "director", "writer", "country"]:
            if field in data.keys() and len(data[field]):
                words_regex = [{"$regex": word, "$options": "i"} for word in data[field].split()]
                if "$and" in query.keys():
                    query["$and"] += [{field: regex} for regex in words_regex]
                else:
                    query["$and"] = [{field: regex} for regex in words_regex]

        for field in [("year", 1900, 2050), ("rating", 1, 10), ("runtime", 1, 999)]:  # key name, lower and upper border
            if field[0] in data.keys():
                if ("from" in data[field[0]].keys()) and data[field[0]]["from"] and (data[field[0]]["from"] >= field[1]):
                    if field[0] not in query.keys():
                        query[field[0]] = {}
                    query[field[0]]["$gte"] = data[field[0]]["from"]
                if ("to" in data[field[0]].keys()) and data[field[0]]["to"] and (data[field[0]]["to"] <= field[2]):
                    if field[0] not in query.keys():
                        query[field[0]] = {}
                    query[field[0]]["$lte"] = data[field[0]]["to"]

        if "sort" in data.keys():
            if len(data["sort"]):
                if data["sort"] == "year-asc":
                    sortby["field"] = "year"
                    sortby["direction"] = 1
                elif data["sort"] == "year-desc":
                    sortby["field"] = "year"
                    sortby["direction"] = -1
                elif data["sort"] == "title-asc":
                    sortby["field"] = "title"
                    sortby["direction"] = 1
                elif data["sort"] == "title-desc":
                    sortby["field"] = "title"
                    sortby["direction"] = -1
                elif data["sort"] == "imdb-asc":
                    sortby["field"] = "rating"
                    sortby["direction"] = 1
                elif data["sort"] == "imdb-desc":
                    sortby["field"] = "rating"
                    sortby["direction"] = -1
            else:
                sortby["field"] = "title"
                sortby["direction"] = 1

        self.listed_films = []
        if len(query.keys()):
            count = service.MongoDB.get_count(query)
            self.page_all = count / self.num_of_movies
            if (count % self.num_of_movies) != 0:
                self.page_all += 1
            self.page_actual = data["pageActual"]
            skip = self.num_of_movies * (self.page_actual - 1)
            limit = self.num_of_movies
            self.listed_films = [movie.movie for movie in service.MongoDB.search_by_query(query, sortby, skip, limit)]
        else:
            self.listed_films = [movie.movie for movie in service.MongoDB.get_random_docs(12)]
            self.page_all = 1
            self.page_actual = 1
        for data in self.listed_films:
            print data
            data["available"] = service.is_available(data["path"])
        print query
