from pymongo import MongoClient
import os
import re
import json
import random

from movie import Movie
from find_my_films import findMyFilms


class MongoDB(object):

    def __init__(self, conn_link):
        try:
            self.client = MongoClient(conn_link)
            self.db = self.client.findmyfilms
            self.data = self.db.data
        except:
            print "Wrong MongoDB connection link"

    def init_connection_link(self, conn_link):
        self.client = MongoClient(conn_link)
        self.db = self.client.findmyfilms
        self.data = self.db.data

    def get_modal_format(self, datalist):
        for data in datalist:
            for key in data.movie.keys():
                if isinstance(data.movie[key], list):
                    if key == "plot":
                        data.movie[key] = data.movie[key][0]
                    elif key == "videofiles":
                        continue
                    else:
                        if len(data.movie[key]) > 4:
                            data.movie[key] = ", ".join(data.movie[key][0:5])
                        else:
                            data.movie[key] = ", ".join(data.movie[key])
        for (data, _id) in zip(datalist, range(0, len(datalist))):
            data.movie["_id"] = _id
        return datalist


    def drop_documents_based_on_locs(self, available_locs):
        for loc in available_locs:
            self.drop_document_based_on_loc(loc)

    def drop_document_based_on_loc(self, location):
        #regex = location.replace("[", ".[").replace("]", ".]")
        regex = re.compile(str(location.replace("[", ".[").replace("]", ".]")).encode("string-escape"))
        self.data.remove({"path": {"$regex": regex}})

    def add(self, json_data):
        try:
            self.data.insert_one(json_data)
        except Exception, e:
            print str(e)

    def add_docs_based_on_root(self, location):
        for root, dirNames, fileNames in os.walk(location):
            regex = re.compile(".*("+findMyFilms.json_fine_file_name+").*")
            json_file = [m.group(0) for l in fileNames for m in [regex.search(l)] if m]
            if len(json_file) == 1:
                with open(os.path.join(root, findMyFilms.json_fine_file_name)) as info_file:
                    # print info_file.read()
                    self.add(json.load(info_file))
            regex = re.compile(".*("+findMyFilms.temp_json_file_name+").*")
            json_file = [m.group(0) for l in fileNames for m in [regex.search(l)] if m]
            if len(json_file) == 1:
                with open(os.path.join(root, findMyFilms.temp_json_file_name)) as info_file:
                    # print info_file.read()
                    self.add(json.load(info_file))

    def get_all(self):
        result = []
        for item in self.data.find():
            movie = Movie()
            movie.set_dict_from_db(item)
            result.append(movie)
        return self.get_modal_format(result)

    def get_random_docs(self, number):
        try:
            random_numbers = []
            count = self.data.count()
            if count < number:
                number = count
            while len(random_numbers) < number:
                rand = random.randrange(0, count)
                if rand not in random_numbers:
                    random_numbers.append(rand)
            result = []
            for number in random_numbers:
                movie = Movie()
                movie.set_dict_from_db(self.data.find({}, {'_id': False}).limit(1).skip(number).next())
                result.append(movie)
            return self.get_modal_format(result)
        except:
            return []

    def get_categories(self):
        try:
            return sorted(self.data.aggregate([
                {"$project": {"category": 1, "a": "$genres"}},
                {"$unwind": "$a"},
                {"$group": {"_id": 'category', "items": {"$addToSet": "$a"}}}
                ]).next()["items"])
        except:
            return []

    def search_by_query(self, query, sort, skip, limit):
        result = []
        for item in self.data.find(query, {'_id': False}).sort(sort["field"], sort["direction"]).skip(skip).limit(limit):
            movie = Movie()
            movie.set_dict_from_db(item)
            result.append(movie)
        return self.get_modal_format(result)

    def get_not_sure_docs(self, all):
        if all == "true":
            return list(self.data.find({}, {"_id": False,"imdblink": True, "path": True, "isidvalid": True}))
        else:
            return list(self.data.find({"isidvalid": False}, {"_id": False,"imdblink": True, "path": True, "isidvalid": True}))

    def update_video_seen(self, data):
        self.data.update({"path": data["path"]}, {"$set": {"videofiles": data["videofiles"]}})

    def get_count(self, query):
        return self.data.find(query).count()
