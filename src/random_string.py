import random
import string


class RandomString:

    def get_random_string(self):
        return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))

RandomString = RandomString()
