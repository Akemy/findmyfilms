(function(){
angular
    .module('findFilmsApp', [])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/',{
                templateUrl: '../static/html/fmf_gray.html',   //findmyfilms
                controller: 'FindFilmsController'
            })
            .when('/settings',{
                templateUrl: '../static/html/settings.html',
                controller: 'SettingsController'
            })
            .when('/update-all', {
                templateUrl: '../static/html/fmf_gray.html',   //findmyfilms
                controller: 'UpdateAllController'
            })
            .when('/update-new', {
                templateUrl: '../static/html/update.html',   //findmyfilms
                controller: 'UpdateNewController'
            })
            .when('/not-sure', {
                templateUrl: '../static/html/notsure.html',
                controller: 'NotSureController'
            })
            .otherwise({ redirectTo: '/' })
    }])
    .service('FindFilmsService', function($http){
        this.initData = function(){
            console.log('initData called');
            return $http.get('/findmyfilms');
        };
    })
    .service('SettingsService', function($http){
        this.initData = function(){
            console.log('Settings: initData called');
            return $http.get('/settings');
        };
    })
    .service('NotSureService', function($http){
        this.initData = function(all){
            console.log('Settings: initData called');
            return $http.get('/not-sure?all=' + all);
        };
    })
    .controller('FindFilmsController', [
        '$scope', '$http', 'FindFilmsService',
        function($scope, $http, FindFilmsService){
            $scope.state = {};
            $scope.params = {};
            $scope.pageName = "findfilms";
            $scope.listedFilms = [];
            $scope.params.title = "";
            $scope.params.paths = [];
            $scope.params.categories = [];
            $scope.params.sort = "title-asc";
            $scope.params.year = {"from": null, "to": null};
            $scope.params.rating = {"from": null, "to": null};
            $scope.params.runtime = {"from": null, "to": null};
            $scope.params.director = "";
            $scope.params.writer = "";
            $scope.params.cast = "";
            $scope.params.country = "";
            $scope.started = false;
            $scope.state.showSort = false;
            $scope.state.advSearch = false;
            $scope.params.pageActual = 1;
            $scope.params.pageAll = 1;

            init();
            function init() {
                FindFilmsService.initData().then(function(data){
                    $scope.listedFilms = data.data.listedFilms;
                    $scope.params.paths = data.data.paths;
                    $scope.params.categories = data.data.categories;
                    $scope.params.pageAll = data.data.pageAll;
                    console.log ( 'success and requested data' );
                    console.log( data  );
                });
            }
            $scope.search = function(){
                console.log("Search, yes...");
                console.log($scope.params);
                $http
                    .post('/search', {
                        search: $scope.params
                    })
                    .success(function(data, status, headers, config){
                        $scope.listedFilms = data.listedFilms;
                        $scope.params.pageAll = data.pageAll;
                        $scope.params.pageActual = data.pageActual;
                    })
                    .error(function(data, status, headers, config){
                    })
            };
            $scope.pagination = function(page){
                if (page > 0 && page <= $scope.params.pageAll){
                    $scope.params.pageActual = page;
                    $scope.search();
                }
            }
            $scope.play = function(file){
                $http
                    .post('/play', {
                        play: file
                    })
                    .success(function(data, status, headers, config){
                    })
                    .error(function(data, status, headers, config){
                    })
            };
            $scope.save = function(film){
                $http
                    .post('/save', {
                        file: film
                    })
                    .success(function(data, status, headers, config){
                    })
                    .error(function(data, status, headers, config){
                    })
            };
            $scope.setSort = function(sort){
                console.log("set_sort:", sort);
                $scope.params.sort = sort;
                console.log("Btw:", $scope.params.sort);
                $scope.search();
            };
            $scope.getImgSrc = function(film) {
                if (film.cover == "None") {
                    return "../static/cover/Unknown.jpg"
                } else if (film.imdbid == "Unknown") {
                    return "../static/cover/" + film.imdbid + ".jpg"
                } else {
                    return "../static/cover/temp/" + film.imdbid + ".jpg"
                }
            };
        }
    ])
    .controller('SettingsController', [
        '$scope', '$http', 'SettingsService',
        function($scope, $http, SettingsService){
            $scope.state = {};
            $scope.state.pageName = "settings";
            $scope.state.connectionLink = "";
            $scope.state.paths = [];
            $scope.state.player = "";
            $scope.state.saved = false;
            $scope.state.error = false;

            init();
            function init() {
                SettingsService.initData().then(function(data){
                    $scope.state.connectionLink = data.data.connectionLink;
                    $scope.state.paths = data.data.paths;
                    $scope.state.player = data.data.player;
                    console.log ( 'Settings: success and requested data' );
                    console.log( data  );
                });
            }
            $scope.savedFalse = function() {
                $scope.state.saved = false;
            };
            $scope.add = function(){
                $scope.savedFalse();
                $scope.state.paths.push({"name": ""});
            };
            $scope.delete = function(path){
                $scope.savedFalse();
                $scope.state.paths.splice($scope.state.paths.indexOf(path),1);
            };
            $scope.save = function(){
                $scope.savedFalse();
                $scope.state.error = false;
                console.log($scope.state.player);
                $http
                    .post('/settings', {
                        connectionLink: $scope.state.connectionLink,
                        paths: $scope.state.paths,
                        player: $scope.state.player
                    })
                    .success(function(data, status, headers, config){
                        $scope.state.saved = true;
                    })
                    .error(function(data, status, headers, config){
                        $scope.state.error = true;
                    });
                alert("You should update the database.");
            }
        }
    ])
    .controller('UpdateAllController', [
        '$http', '$window', function($http, $window){
            init();
            function init() {
                $http
                    .get('/update-all')
                    .success(function(data, status, headers, config){
                        console.log("succ");
                    })
                    .error(function(data, status, headers, config){
                        console.log("fail");
                    })
                ;
                $window.location.href = '/';
            }
        }
    ])
    .controller('UpdateNewController', [
        '$http', '$window', function($http, $window){
            init();
            function init() {
                $http
                    .get('/update-new')
                    .success(function(data, status, headers, config){
                        console.log("succ");
                    })
                    .error(function(data, status, headers, config){
                        console.log("fail");
                    })
                ;
                //$window.location.href = '/';
            }
        }
    ])
    .controller('NotSureController', [
        '$scope', '$http', 'NotSureService',
        function($scope, $http, NotSureService){
            $scope.state = {}
            $scope.state.filmList = []
            $scope.state.saved = false;
            $scope.state.all = false;

            init();
            function init() {
                NotSureService.initData(false).then(function(data){
                    $scope.state.filmList = data.data.filmList
                    console.log ( 'success and requested data' );
                    console.log( data  );
                });
            };

            $scope.savedFalse = function() {
                $scope.state.saved = false;
            };

            $scope.state.show = function() {
                if ($scope.state.filmList.length == 0) {
                    return true;
                }
                return false;
            };

            $scope.state.save = function(){
                $scope.savedFalse();
                data = []
                console.log('post data:')
                console.log($scope.state.filmList.filter(function(x) {return x.changed && x.isidvalid}));
                data = $scope.state.filmList.filter(function(x) {return x.changed && x.isidvalid});
                $http
                    .post('/not-sure', {
                        validFilmList: data
                    })
                    .success(function(data, status, headers, config){
                        $scope.state.saved = true;
                    })
                    .error(function(data, status, headers, config){
                    });
            };

            $scope.state.update = function() {
                $scope.state.all = !$scope.state.all;
                NotSureService.initData($scope.state.all).then(function(data){
                    $scope.state.filmList = data.data.filmList
                    console.log ( 'success and requested data' );
                    console.log( data  );
                });
            }

            $scope.state.changed = function(film) {
                film.changed = true;
            }
        }
    ])
    .directive('navigationbar', function() {
        return {
            restrict: 'E',
            templateUrl: '../static/html/navigationbar.html'
        }
    })
    .directive('search', function(){
        return {
            restrict: 'E',
            templateUrl: '../static/html/search.html'
        }
    })
    .directive('modal', function(){
        return {
            restrict: 'E',
            templateUrl: '../static/html/modal.html'
        }
    })
    .controller('HeaderController', ["$scope", "$location",
        function($scope, $location){
            $scope.isActive = function (viewLocation) {
                return viewLocation === $location.path();
            };
         }
    ])
    .directive('navtabs', function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: '../static/html/navtabs.html',
            scope: {
                pageName: '='
            },
            controller: [
                '$scope',
                function($scope) {
                    this.selectTabIfOnPage = function(tab) {
                        if (tab.name === $scope.state.pageName) {
                            tab.selected = true;
                        }
                    };
                }
            ]
        };
    })
    .directive('tab', function() {
        return {
            require: '^navtabs',
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {},
            template: '<li ng-class="{ active: selected }"><a href="{{ href }}" ng-transclude></a></li>',
            link: function(scope, element, attr, navtabsCtrl) {
                scope.name = attr.name;
                scope.href = attr.href;
                scope.selected = false;
                navtabsCtrl.selectTabIfOnPage(scope);
            }
        };
    })
;
})();